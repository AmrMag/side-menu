//
//  ViewController.swift
//  SlideOutMenu
//
//  Created by Yash Patel on 11/10/17.
//  Copyright © 2017 Yash Patel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var sideView: UIView!
    var isSlideMenuHidden = true
    
    var menuWidth : CGFloat?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        menuWidth = sideView.frame.width
        
        
        // initialize constant with 0
        sideMenuConstraint.constant = 0 - menuWidth!
    }

  
    @IBAction func organizeBtnPressed(_ sender: UIBarButtonItem) {
        
        if isSlideMenuHidden {
            sideMenuConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
            
        }else {
            
            sideMenuConstraint.constant = 0 - menuWidth!
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        isSlideMenuHidden = !isSlideMenuHidden
        
    }
    
    
    
    
    
    
    
    
    
    
    
}

